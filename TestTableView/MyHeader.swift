//
//  MyHeader.swift
//  TestTableView
//
//  Created by Kevin Huang on 2017/3/20.
//  Copyright © 2017年 Kevin Huang. All rights reserved.
//

import UIKit

protocol UserHeaderTableViewCellDelegate {
    func didSelectUserHeaderTableViewCell(Selected: Bool, UserHeader: MyHeader)
}

class MyHeader: UITableViewCell {
    
    var delegate : UserHeaderTableViewCellDelegate?

    @IBOutlet weak var section_title: UILabel!
    
    @IBOutlet weak var btn: UIButton!
    
    var sectionIndex : Int!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

    @IBAction func SelectHeader(_ sender: Any) {
        delegate?.didSelectUserHeaderTableViewCell(Selected: true, UserHeader: self)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
