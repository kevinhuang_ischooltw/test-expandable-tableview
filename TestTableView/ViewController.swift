//
//  ViewController.swift
//  TestTableView
//
//  Created by Kevin Huang on 2017/3/20.
//  Copyright © 2017年 Kevin Huang. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var table_data = Array<TableData>()
    var selectedSections = Set<Int>()
    
    struct TableData
    {
        var section:String = ""
        var data = Array<String>()
        init(){}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.dataSource = self
        tableView.delegate = self
        
        self.tableView.estimatedSectionHeaderHeight = 80
        
        var new_elements:TableData
        
        new_elements = TableData()
        new_elements.section = "Section 1"
        new_elements.data.append("Element 1")
        new_elements.data.append("Element 2")
        new_elements.data.append("Element 3")
        new_elements.data.append("Element 4")
        new_elements.data.append("Element 5")
        new_elements.data.append("Element 6")
        new_elements.data.append("Element 7")
        
        table_data.append(new_elements)
        
        
        new_elements = TableData()
        new_elements.section = "Section 2"
        new_elements.data.append("Element 1")
        new_elements.data.append("Element 2")
        
        table_data.append(new_elements)
        
        new_elements = TableData()
        new_elements.section = "Section 3"
        new_elements.data.append("Element 1")
        new_elements.data.append("Element 2")
        new_elements.data.append("Element 3")
        new_elements.data.append("Element 4")
        new_elements.data.append("Element 5")
        new_elements.data.append("Element 6")
        new_elements.data.append("Element 7")
        
        table_data.append(new_elements)
        
        new_elements = TableData()
        new_elements.section = "Section 4"
        new_elements.data.append("Element 1")
        new_elements.data.append("Element 2")
        new_elements.data.append("Element 3")
        new_elements.data.append("Element 4")
        new_elements.data.append("Element 5")
        new_elements.data.append("Element 6")
        new_elements.data.append("Element 7")
        
        table_data.append(new_elements)

        
        new_elements = TableData()
        new_elements.section = "Section 5"
        new_elements.data.append("Element 1")
        new_elements.data.append("Element 2")
        new_elements.data.append("Element 3")
        new_elements.data.append("Element 4")
        new_elements.data.append("Element 5")
        new_elements.data.append("Element 6")
        new_elements.data.append("Element 7")
        
        table_data.append(new_elements)

        
        new_elements = TableData()
        new_elements.section = "Section 6"
        new_elements.data.append("Element 1")
        new_elements.data.append("Element 2")
        new_elements.data.append("Element 3")
        new_elements.data.append("Element 4")
        new_elements.data.append("Element 5")
        new_elements.data.append("Element 6")
        new_elements.data.append("Element 7")
        
        table_data.append(new_elements)

    }

//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! MyCell
        
        cell.lblDate.text = table_data[indexPath.section].data[indexPath.row]
        cell.lblMsg.text = String(indexPath.row)
        
        
        cell.isHidden = (!selectedSections.contains(indexPath.section) )
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if  selectedSections.contains(indexPath.section) {
            return 40.0
        }
        else {
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return table_data[section].data.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return table_data.count
    }
    
    //Section Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "header") as! MyHeader
        
        headerCell.section_title.text = table_data[section].section
        headerCell.backgroundColor =  UIColor(red:72/255,green:141/255,blue:200/255,alpha:0.9)
        
        headerCell.delegate = self
        headerCell.sectionIndex = section
        
        
        return headerCell

    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
//    func sectionIndexTitlesForTableView(tableView: UITableView) -> [AnyObject]!
//    {
//        let sections_names = table_data.map { $0.section  }
//        return sections_names
//        
//    }
//    

}

extension ViewController : UserHeaderTableViewCellDelegate {
    func didSelectUserHeaderTableViewCell(Selected: Bool, UserHeader: MyHeader) {
        print("section \(UserHeader.sectionIndex!) is clicked ...")
        if let sectionIndex = UserHeader.sectionIndex {
            if selectedSections.contains(sectionIndex) {
                selectedSections.remove(sectionIndex)
            }
            else {
                selectedSections.insert(sectionIndex)
            }
            tableView.reloadData()
        }
    }
}

